/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import objetos.objEmpleado;
import negocio.empleado;

/**
 *
 * @author manue
 */
public class archivo {

    public void InsertarEnArchivo1(ArrayList<objEmpleado> listaEmpleados) {
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, false));
            String datos = "";
            for (int i = 0; i < listaEmpleados.size(); i++) {
                datos = "";
                String cedula = listaEmpleados.get(i).getCedula();
                String nombre = listaEmpleados.get(i).getNombre();
                String puesto = listaEmpleados.get(i).getPuesto();
                String genero = listaEmpleados.get(i).getGenero();
                String departamento = listaEmpleados.get(i).getDepartamento();
                String nivelIngles = listaEmpleados.get(i).getNivelIngles();
                String fechaNaci = listaEmpleados.get(i).getFechaNaci();
                String edad = listaEmpleados.get(i).getEdad();
                String fechaIngre = listaEmpleados.get(i).getFechaIngre();
                datos = cedula + "," + nombre + "," + puesto + "," + genero + "," + departamento + "," + 
                        nivelIngles + "," + fechaNaci + "," + edad + "," + fechaIngre;
                archi.write(datos + "\r\n");
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static ArrayList<objEmpleado> LeerDesdeArchivoObjeto() {
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            objEmpleado.listaEmpleados=new ArrayList();
            while (archi.ready()) {
                String[] separar = new String[9];
                separar = archi.readLine().split(",");
                objEmpleado.listaEmpleados.add(new objEmpleado(separar[0], separar[1], separar[2], separar[3], separar[4], separar[5], 
                        separar[6], separar[7], separar[8]));
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return objEmpleado.listaEmpleados;
    }
    
    public void InsertarEnArchivo (String datosEmpleado){
        try{
            File archivo = new File("ListaUsuarios.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datosEmpleado + "\r\n");
            archi.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
