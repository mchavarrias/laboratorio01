/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import negocio.empleado;
import objetos.objEmpleado;

/**
 *
 * @author manue
 */
public class frmModificar extends javax.swing.JFrame {

    empleado editarEmpleado = new empleado();

    /**
     * Creates new form frmModificar
     */
    public frmModificar() {
        initComponents();
        mostras();
        mostarCedula();
        edadText.setEditable(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dFechaIngre = new com.toedter.calendar.JDateChooser();
        jbDepartamento = new javax.swing.JComboBox<>();
        edadText = new javax.swing.JTextField();
        genero = new javax.swing.JLabel();
        nivelIngles = new javax.swing.JLabel();
        btnMostrar = new javax.swing.JButton();
        jbNivelIngles = new javax.swing.JComboBox<>();
        fechaDeNacimiento = new javax.swing.JLabel();
        fechaDeNacimiento1 = new javax.swing.JLabel();
        fechaDeNacimiento2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        nombreText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jbPuesto = new javax.swing.JComboBox<>();
        puesto = new javax.swing.JLabel();
        departamento = new javax.swing.JLabel();
        dNacimiento = new com.toedter.calendar.JDateChooser();
        btnModifi = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla = new javax.swing.JTable();
        btnRegresar = new javax.swing.JButton();
        jbGenero = new javax.swing.JComboBox<>();
        cbxCedula = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jbDepartamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Produccion", "Aseguramiento de la Calidad", "Data Entry" }));

        genero.setText("Genero");

        nivelIngles.setText("Nivel Ingles");

        btnMostrar.setText("Mostrar");

        jbNivelIngles.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "A1", "A2", "B1", "B2", "C1" }));

        fechaDeNacimiento.setText("Fecha de Nacimiento");

        fechaDeNacimiento1.setText("Fecha de Ingreso");

        fechaDeNacimiento2.setText("Edad");

        jLabel1.setText("Nombre");

        nombreText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreTextActionPerformed(evt);
            }
        });

        jLabel2.setText("Cedula");

        jbPuesto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ingeniero en Software", "Senior", "Ingeniero de Calidad", "Arquitecto" }));

        puesto.setText("Puesto");

        departamento.setText("Departamento");

        btnModifi.setText("Modificar");
        btnModifi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifiActionPerformed(evt);
            }
        });

        Tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Cedula", "Nombre", "Genero", "Departamento", "Puesto", "Nivel Ingles", "Edad", "Fecha Nacimiento", "Fecha Ingreso"
            }
        ));
        jScrollPane1.setViewportView(Tabla);

        btnRegresar.setText("Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        jbGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Femenino", "Masculino", "Indefinido" }));

        cbxCedula.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxCedulaItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(genero, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jbGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nombreText, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                            .addComponent(cbxCedula, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(puesto, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(nivelIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(departamento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jbNivelIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fechaDeNacimiento)
                            .addComponent(fechaDeNacimiento1)
                            .addComponent(fechaDeNacimiento2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dNacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dFechaIngre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edadText, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(55, 55, 55))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnModifi, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dNacimiento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jbPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(puesto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fechaDeNacimiento)
                        .addComponent(jLabel2)
                        .addComponent(cbxCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechaDeNacimiento2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edadText)
                            .addComponent(departamento, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(nombreText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(genero, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbNivelIngles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechaDeNacimiento1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nivelIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbGenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(59, 59, 59))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(dFechaIngre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnModifi, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRegresar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void ingresarDato() throws BadLocationException {
        ArrayList<objEmpleado> listaEmpleados = new ArrayList<objEmpleado>();
        listaEmpleados = editarEmpleado.LeerUsuariosObjetos();
        for (int i = 0; i < listaEmpleados.size(); i++) {
            if (listaEmpleados.get(i).getCedula().equals(cbxCedula.getSelectedItem())) {
                listaEmpleados.remove(i);
                String cedula = (String) cbxCedula.getSelectedItem();
                String nombre = nombreText.getText();
                String genero = jbGenero.getSelectedItem().toString();
                String departamento = jbDepartamento.getSelectedItem().toString();
                String fechaNaci = "";
                String edad = edadText.getText();
                String fechaIngre = "";
                String nivelIngles = jbNivelIngles.getSelectedItem().toString();
                String puesto = jbPuesto.getSelectedItem().toString();
                try {
                    DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                    fechaIngre = f.format(dFechaIngre.getDate());
                    fechaNaci = f.format(dNacimiento.getDate());

                } catch (Exception e) {
                }
                objEmpleado.listaEmpleados.add(new objEmpleado(cedula,nombre, puesto, genero, departamento, nivelIngles, fechaNaci, edad, fechaIngre));
                break;
            }
        }
        editarEmpleado.guardar(listaEmpleados);
    }

    public void mostarseleccionado() {
        ArrayList<objEmpleado> listaEmpleados = new ArrayList<objEmpleado>();
        listaEmpleados = editarEmpleado.LeerUsuariosObjetos();

        for (int i = 0; i < listaEmpleados.size(); i++) {
            if (listaEmpleados.get(i).getCedula().equals(cbxCedula.getSelectedItem())) {
                nombreText.setText(listaEmpleados.get(i).getNombre());
                jbGenero.setSelectedItem(listaEmpleados.get(i).getGenero());
                jbDepartamento.setSelectedItem(listaEmpleados.get(i).getDepartamento());
                jbNivelIngles.setSelectedItem(listaEmpleados.get(i).getNivelIngles());
                jbPuesto.setSelectedItem(listaEmpleados.get(i).getPuesto());
                edadText.setText(listaEmpleados.get(i).getEdad());
                try {
                    Date fecha1 = null, fecha2 = null;
                    SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                    fecha1 = f.parse(listaEmpleados.get(i).getFechaIngre());
                    fecha2 = f.parse(listaEmpleados.get(i).getFechaNaci());
                    dFechaIngre.setDate(fecha1);
                    dNacimiento.setDate(fecha2);
                } catch (Exception e) {
                }
            }
        }
    }

    public void mostarCedula() {
        ArrayList<objEmpleado> listaEmpleados = new ArrayList<objEmpleado>();
        listaEmpleados = editarEmpleado.LeerUsuariosObjetos();

        for (int i = 0; i < listaEmpleados.size(); i++) {
            cbxCedula.addItem(listaEmpleados.get(i).getCedula());
        }
    }

    public void mostras() {
        ArrayList<objEmpleado> listaEmpleados = new ArrayList<objEmpleado>();
        listaEmpleados = editarEmpleado.LeerUsuariosObjetos();
        DefaultTableModel model = (DefaultTableModel) Tabla.getModel();
        model.setRowCount(0);
        Object[] arreglo = new Object[9];
        for (int i = 0; i < listaEmpleados.size(); i++) {
            arreglo[0] = listaEmpleados.get(i).getCedula();
            arreglo[1] = listaEmpleados.get(i).getNombre();
            arreglo[2] = listaEmpleados.get(i).getGenero();
            arreglo[3] = listaEmpleados.get(i).getDepartamento();
            arreglo[4] = listaEmpleados.get(i).getPuesto();
            arreglo[5] = listaEmpleados.get(i).getNivelIngles();
            arreglo[6] = listaEmpleados.get(i).getEdad();
            arreglo[7] = listaEmpleados.get(i).getFechaIngre();
            arreglo[8] = listaEmpleados.get(i).getFechaNaci();
            model.addRow(arreglo);
        }
    }
    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        frmprincipal regresar = new frmprincipal();
        regresar.setVisible(true);
    }//GEN-LAST:event_btnRegresarActionPerformed

    private void nombreTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreTextActionPerformed

    private void cbxCedulaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxCedulaItemStateChanged
        mostarseleccionado();
    }//GEN-LAST:event_cbxCedulaItemStateChanged

    private void btnModifiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifiActionPerformed

        try {
            ingresarDato();
        } catch (BadLocationException ex) {
            Logger.getLogger(frmModificar.class.getName()).log(Level.SEVERE, null, ex);
        }
        mostras();
    }//GEN-LAST:event_btnModifiActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmModificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmModificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmModificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmModificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmModificar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Tabla;
    private javax.swing.JButton btnModifi;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JComboBox<String> cbxCedula;
    private com.toedter.calendar.JDateChooser dFechaIngre;
    private com.toedter.calendar.JDateChooser dNacimiento;
    private javax.swing.JLabel departamento;
    private javax.swing.JTextField edadText;
    private javax.swing.JLabel fechaDeNacimiento;
    private javax.swing.JLabel fechaDeNacimiento1;
    private javax.swing.JLabel fechaDeNacimiento2;
    private javax.swing.JLabel genero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jbDepartamento;
    private javax.swing.JComboBox<String> jbGenero;
    private javax.swing.JComboBox<String> jbNivelIngles;
    private javax.swing.JComboBox<String> jbPuesto;
    private javax.swing.JLabel nivelIngles;
    private javax.swing.JTextField nombreText;
    private javax.swing.JLabel puesto;
    // End of variables declaration//GEN-END:variables
}
