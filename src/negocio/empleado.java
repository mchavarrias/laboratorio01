/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import datos.archivo;
import objetos.objEmpleado;

/**
 *
 * @author manue
 */
public class empleado {

    archivo datoUsuario = new archivo();

   

    public void InsertarUsuario(ArrayList<objEmpleado> listaEmpleados) {
        String dato = "";
        for (int i = 0; i <= listaEmpleados.size() - 1; i++) {

            String nombre = listaEmpleados.get(i).getNombre();
            String cedula = listaEmpleados.get(i).getCedula();
            String puesto = listaEmpleados.get(i).getPuesto();
            String genero = listaEmpleados.get(i).getGenero();
            String departamento = listaEmpleados.get(i).getDepartamento();
            String nivelIngles = listaEmpleados.get(i).getNivelIngles();
            String fechaNaci = listaEmpleados.get(i).getFechaNaci();
            String edad = listaEmpleados.get(i).getEdad();
            String fechaIngre = listaEmpleados.get(i).getFechaIngre();
            dato = nombre + "," + cedula + "," + puesto + "," + genero + "," + departamento + "," + nivelIngles + "," + fechaNaci + "," + edad + "," + fechaIngre;
        }

        datoUsuario.InsertarEnArchivo(dato);
    }

    public ArrayList<objEmpleado> LeerUsuariosObjetos() {
        ArrayList<objEmpleado> listaEmpleados = new ArrayList<objEmpleado>();
        listaEmpleados = datoUsuario.LeerDesdeArchivoObjeto();
        return listaEmpleados;
    }
    
    public void guardar(ArrayList<objEmpleado> listaEmpleados){
        datoUsuario.InsertarEnArchivo1(listaEmpleados);
    }
}
