/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.ArrayList;

/**
 *
 * @author manue
 */
public class objEmpleado {

    private String nombre;
    private String cedula;
    private String puesto;
    private String genero;
    private String departamento;
    private String nivelIngles;
    private String fechaNaci;
    private String edad;
    private String fechaIngre;

    public objEmpleado(String cedula, String nombre, String puesto, String genero, String departamento, String nivelIngles, String fechaNaci, String edad, String fechaIngre) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.puesto = puesto;
        this.genero = genero;
        this.departamento = departamento;
        this.nivelIngles = nivelIngles;
        this.fechaNaci = fechaNaci;
        this.edad = edad;
        this.fechaIngre = fechaIngre;
    }

    public static ArrayList listaEmpleados = new ArrayList<>();
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getNivelIngles() {
        return nivelIngles;
    }

    public void setNivelIngles(String nivelIngles) {
        this.nivelIngles = nivelIngles;
    }

    public String getFechaNaci() {
        return fechaNaci;
    }

    public void setFechaNaci(String fechaNaci) {
        this.fechaNaci = fechaNaci;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getFechaIngre() {
        return fechaIngre;
    }

    public void setFechaIngre(String fechaIngre) {
        this.fechaIngre = fechaIngre;
    }
    
    
}